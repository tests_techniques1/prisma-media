class Parallax {
    /**
     * Need ration and direction attributes.
     * Here we will observe the visibility of the parent of the components under the effect of parallax in order to make them move only if the parent is visible.
     * @param {HTMLElement} element
     */
    constructor(element) {
        this.element = element;
        this.ratio = this.element.getAttribute('ratio');
        this.direction = this.element.getAttribute('direction');
        this.checkAttributes = this.checkAttributes.bind(this);
        if (!this.checkAttributes()) return;
        this.moveElement = this.moveElement.bind(this);
        this.onIntersection = this.onIntersection.bind(this);
        const observer = new IntersectionObserver(this.onIntersection);
        observer.observe(element.parentElement);
    }


    /**
     * Print the error string
     * @param {string} errorString
     */
    attributesError(errorString) {
        console.error('[CHECK-ATTRIBUTES] ' + errorString);
    }

    /**
     * Checks if ratio and direction attributes are valid and return true if yes else false.
     * @returns {boolean}
     */
    checkAttributes() {
        let directions = ['top', 'left', 'bottom', 'right'];
        if (!this.ratio) {
            this.attributesError('Null attribute ratio parallax');
            return false;
        }
        if (!this.direction) {
            this.attributesError('Null attribute direction parallax');
            return false;
        }
        if (!this.ratio.match('^-?\\d+(.\\d+)?$')) {
            this.attributesError('No valid ratio attribute parallax');
            return false;
        }
        if (!directions.includes(this.direction)) {
            this.attributesError('No valid direction attribute  parallax');
            return false;
        }
        return true;
    }

    /**
     * See if the element is visible or not. If yes then we listen to the scroll event otherwise we stop listening.
     * @param {IntersectionObserverEntry[]} entries
     */
    onIntersection(entries) {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                document.addEventListener('scroll', this.moveElement);
            } else {
                document.removeEventListener('scroll', this.moveElement);
            }
        });
    }

    /**
     * Allows to call this function only when the page is redrawn.
     * Play parallax to the good direction
     */
    moveElement() {
        window.requestAnimationFrame(() => {
            const { scrollY } = window;
            const parentTop = this.element.parentElement.offsetTop;
            const multiply = scrollY < 10 ? scrollY : parentTop - scrollY;
            switch (this.direction) {
                case 'right': {
                    this.element.setAttribute('style', `transform: translateX(${this.ratio * multiply}px)`)
                    break;
                }
                case 'left' : {
                    this.element.setAttribute('style', `transform: translateX(${this.ratio * -multiply}px)`)
                    break;
                }
                case 'top': {
                    this.element.setAttribute('style', `transform: translateY(${this.ratio * -multiply}px)`)
                    break;
                }
                case 'bottom': {
                    this.element.setAttribute('style', `transform: translateY(${this.ratio * multiply}px)`)
                    break;
                }
            }
        })
    }

    /**
     * Search in the document all the elements having the attribute "parallax" and create for each element found a Parallax object of it.
     * @returns {Parallax[]}
     */
    static bind() {
        return Array.from(document.querySelectorAll('[parallax]')).map(element => {
            return new Parallax(element);
        })
    }
}

/**
 * Waiting for the whole DOM to be loaded with images etc.
 */
addEventListener("load", () => {
    Parallax.bind();
})
