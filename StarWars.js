let isClicked = false;
/**
 * This function is triggered by click event of the menu.png image.
 * It will check if the burger menu is already open thanks to isClicked variable.
 * - If not it will set the image to close.png and make appear the burger menu.
 * - Else it will set the image to menu.png and close the burger menu.
 */
function openBurgerMenu() {
    const burgerMenu = document.getElementById("burgerMenu");
    const burgerImage = document.getElementById("burgerImage");
    if (isClicked) {
        burgerImage.setAttribute("src", "Assets/menu.png");
        burgerMenu.setAttribute("style", "display:none");
        isClicked = false;
    } else {
        burgerImage.setAttribute("src", "Assets/close.png");
        burgerMenu.setAttribute("style", "display:inline");
        isClicked = true;
    }
}
